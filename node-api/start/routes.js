'use strict';

const Route = use('Route');

Route.get('/', ({ request }) => {
  return { greeting: 'Hello world in JSON' };
});
Route.get('posts', 'PostController.index');
Route.get('posts/:id', 'PostController.show');
Route.post('posts', 'PostController.store');

Route.get('questions', 'QuestionController.index');
Route.post('questions', 'QuestionController.store');

Route.get('options', 'OptionController.index');
Route.post('options', 'OptionController.store');
