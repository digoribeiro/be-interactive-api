'use strict';
const Post = use('App/Models/Post');

class PostController {
  async index({ response }) {
    const posts = await Post.query()
      .with('questions.options')
      .fetch();
    response.status(200).json({
      message: 'Here are your posts',
      data: posts,
    });
  }

  async show({ params, response }) {
    const post = await Post.query()
      .where('id', params.id)
      .with('questions.options')
      .fetch();
    response.status(200).json({
      message: 'Here is your post.',
      data: post,
    });
  }

  async store({ request, response }) {
    const {
      name,
      description,
      category,
      basePoints,
      startDate,
      endDate,
      isActive,
      alreadyAnswered,
    } = request.post();
    const post = await Post.create({
      name,
      description,
      category,
      basePoints,
      startDate,
      endDate,
      isActive,
      alreadyAnswered,
    });

    response.status(201).json({
      message: 'Successfully created a new post.',
      data: post,
    });
  }
}

module.exports = PostController;
