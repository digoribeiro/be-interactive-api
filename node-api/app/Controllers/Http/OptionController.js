'use strict';
const Option = use('App/Models/Option');

class OptionController {
  async index({ response }) {
    const options = await Option.all();
    response.status(200).json({
      message: 'Here are your options',
      data: options,
    });
  }

  async store({ request, response }) {
    const { text, value, question_id } = request.post();
    const option = await Option.create({
      text,
      value,
      question_id,
    });
    response.status(201).json({
      message: 'Successfully created a new option.',
      data: option,
    });
  }
}

module.exports = OptionController;
