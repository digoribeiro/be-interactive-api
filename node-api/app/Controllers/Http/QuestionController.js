'use strict';
const Question = use('App/Models/Question');

class QuestionController {
  async index({ response }) {
    const questions = await Question.query()
      .with('options')
      .fetch();

    response.status(200).json({
      message: 'Here are your questions',
      data: questions,
    });
  }
  async store({ request, response }) {
    const { text, type, post_id } = request.post();
    const question = await Question.create({
      text,
      type,
      post_id,
    });
    response.status(201).json({
      message: 'Successfully created a new question.',
      data: question,
    });
  }
}

module.exports = QuestionController;
