'use strict';

const Model = use('Model');

class Post extends Model {
  static get hidden() {
    return ['created_at', 'updated_at'];
  }
  questions() {
    return this.hasMany('App/Models/Question');
  }
}

module.exports = Post;
