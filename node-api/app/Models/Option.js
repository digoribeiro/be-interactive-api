'use strict';

const Model = use('Model');

class Option extends Model {
  static get hidden() {
    return ['question_id', 'created_at', 'updated_at'];
  }
}

module.exports = Option;
