'use strict';

const Model = use('Model');

class Question extends Model {
  static get hidden() {
    return ['post_id', 'created_at', 'updated_at'];
  }
  options() {
    return this.hasMany('App/Models/Option');
  }
}

module.exports = Question;
