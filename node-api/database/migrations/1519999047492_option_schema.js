'use strict'

const Schema = use('Schema')

class OptionSchema extends Schema {
  up () {
    this.create('options', (table) => {
      table.increments()
      table.string('text')
      table.string('value')
      table.integer('question_id')
      table.timestamps()
    })
  }

  down () {
    this.drop('options')
  }
}

module.exports = OptionSchema
