'use strict'

const Schema = use('Schema')

class PostSchema extends Schema {
  up () {
    this.create('posts', (table) => {
      table.increments()
      table.string('name')
      table.text('description', 'longtext')
      table.string('category')
      table.string('basePoints')
      table.dateTime('startDate')
      table.dateTime('endDate')
      table.boolean('isActive')
      table.boolean('alreadyAnswered')
      table.timestamps()
    })
  }

  down () {
    this.drop('posts')
  }
}

module.exports = PostSchema
