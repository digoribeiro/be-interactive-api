'use strict';
const Option = use('App/Models/Option');

class OptionSeeder {
  async run() {
    const options = [
      {
        text: 'Front-End',
        value: 'front-end',
        question_id: 2,
      },
      {
        text: 'Back-End',
        value: 'back-end',
        question_id: 2,
      },
    ];
    for (const option of options) {
      await Option.create(option);
    }
  }
}

module.exports = OptionSeeder;
