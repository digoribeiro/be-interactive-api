'use strict';

/*
|--------------------------------------------------------------------------
| PostSeeder
|--------------------------------------------------------------------------
|
| Make use of the Factory instance to seed database with dummy data or
| make use of Lucid models directly.
|
*/

const Factory = use('Factory');
const Post = use('App/Models/Post');

class PostSeeder {
  async run() {
    const posts = [
      {
        name: 'Teste de desenvolvimento',
        description: 'Teste para desenvolvedor back-end',
        category: 'Testes',
        basePoints: '100',
        startDate: '2018-02-15 00:00:00',
        endDate: '2018-02-22 00:00:00',
        isActive: true,
        alreadyAnswered: false,
      },
    ];
    for (const post of posts) {
      await Post.create(post);
    }
  }
}

module.exports = PostSeeder;
