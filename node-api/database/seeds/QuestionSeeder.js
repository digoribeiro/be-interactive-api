'use strict';

const Factory = use('Factory');
const Question = use('App/Models/Question');

class QuestionSeeder {
  async run() {
    const questions = [
      {
        text: 'Qual seu nome?',
        type: 'text',
        post_id: 1,
      },
      {
        text: 'Para qual area voce esta aplicando?',
        type: 'select',
        post_id: 1,
      },
    ];
    for (const question of questions) {
      await Question.create(question);
    }
  }
}

module.exports = QuestionSeeder;
