# API FENG
## Prerequisites

* Git - [Download & Install Git](https://git-scm.com/downloads).

* Node.js - [Download & Install Node.js](https://nodejs.org/en/download/)

* AdonisJs - [Download & Install AdonisJs](npm i -g @adonisjs/cli)

* MySql - [Download & Install MySql](https://dev.mysql.com/downloads/installer/)

### Cloning The GitHub Repository

```bash
git clone https://digoribeiro@bitbucket.org/digoribeiro/be-interactive-api.git
```

To install the dependencies, run this in the application folder from the command-line:

```bash
cd be-interactive-apibe-interactive-api/node-api && npm install
```

## Creating Database and Tables

Rename the .env.example file to .env and enter the settings for your database

Run:

```bash
adonis migration:run
```
### Update tables
```bash
adonis migration:refresh
```

## Creating Post, questions and options

Run:

```bash
adonis seed
```

## Running api

Run api using npm:

```bash
adonis serve --dev
```

Your application should run on port 3333 [http://localhost:3333/posts/1](http://localhost:3333/posts/1)
